package utils;

import java.util.StringTokenizer;

public class MaskMethods {
	
	public static void main(String[] args) {
        	
		 	System.out.println("Metodos para enmascarar: ");
            System.out.println("Telefono: " + maskTelephone("5530588254"));
            System.out.println("RFC: " + maskRFC("RAVO881117SL0"));
            System.out.println("Nombre: " + maskName("Omar De Los Ram�rez Valverde"));
            System.out.println("Nombre x caracter: " + maskNameCharter("Omar De Los Ram�rez Valverde"));            
            System.out.println("Ultimos 4 digitos: " + maskLastFour("557907005789"));
            System.out.println("Fecha: " + maskDate("17/11/1988"));
            System.out.println("Tarjeta: " + maskCard("5579070059698574"));
            System.out.println("Cuenta Clabe: " + maskAccountClabe("123456789098765432"));
            System.out.println("Cuenta de Cheques: " + maskCheckingAccount("123456789098765432"));
            System.out.println("Creditos: " + maskCredits("098765432123"));
            System.out.println("Mail: " + maskMail("omar.ramirez@ids.com.mx"));

    }

	
	public static String maskTelephone(String telephone) {
		telephone = telephone.substring(0,2) + "*****" +  telephone.substring(telephone.length() - 3);
        
		return telephone;
    }
	
	public static String maskRFC(String rfc) {
		rfc = rfc.replaceAll(rfc, "************");
        
		return rfc;
    }
	
	public static String maskDate(String date) {
		date = date.replaceAll(date, "**/**/****");
        
		return date;
    }
	
	public static String maskName(String nombre) {
		StringTokenizer st = new StringTokenizer(nombre);
		int numberWords = st.countTokens();
		 
		String wordFull = "";
		  
		for(int k=0;k<numberWords;k++) {
			String PalabraIndividual=st.nextToken();
		    wordFull += PalabraIndividual.substring(0,1).toUpperCase() + "*****" + " ";
		}
		        
		return wordFull;
    }
	
	public static String maskNameCharter(String nombre) {
		StringTokenizer st =new StringTokenizer(nombre);
		int numberWords = st.countTokens();
		String initials = "";
		String complementName = "";
		String charter = "";
		String fullName = "";
				
		for(int k=0; k<numberWords; k++) {
			String palabraIndividual=st.nextToken();
			complementName  = palabraIndividual.substring(1, palabraIndividual.length()) + " ";  

			for(int i=0; i<palabraIndividual.substring(0,1).length(); i++) {
				
				charter = complementName.replaceAll("[a-zA-Z����������]{1}", "*");
				fullName += palabraIndividual.substring(0,1).toUpperCase() + charter + " ";

			}	
		}
	

		return fullName;
    }
	
	public static String maskLastFour(String count) {
		count = count.replaceAll("\\d(?=\\d{4})", "*");
        
		return count;
    }
	
	public static String maskCard(String card) {
		card = card.substring(0,6) + "*****" +  card.substring(card.length() - 4);

        return card;
    }
	
	public static String maskAccountClabe(String account) {
		account = account.substring(0,3) + "*****" +  account.substring(account.length() - 4);

        return account;
    }
	
	public static String maskCheckingAccount(String checkingAccount) {
		checkingAccount = checkingAccount.substring(0,4) + "*****" +  checkingAccount.substring(checkingAccount.length() - 4);

        return checkingAccount;
    }


	public static String maskCredits(String credits) {
		credits = credits.substring(0,6) + "*****" +  credits.substring(credits.length() - 4);

        return credits;
    }
	
	public static String maskMail(String mail) {

		String[] mailSplit = mail.split("@");
		mail = "*******" + mailSplit[0].substring(mailSplit[0].length() -3) + "@" + mailSplit[1];
        return mail;
    }
	

}
